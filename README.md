This project is about the F-Droid compatible repo running at https://apt.izzysoft.de/fdroid/

There's no code maintained here. The main purpose of this project is to enable
you to actively contribute to the repo (e.g. by suggesting apps that should be
added or removed), as well as to reach out for help. Suggestions for other
improvals are of course welcomed, too.


## What is the IzzyOnDroid Repo?
It is an F-Droid style repository for Android apps, provided by [IzzyOnDroid]. 
Applications in this repository are official binaries built by the original
application developers, taken from their resp. repositories (mostly Github).
You can find a more detailed description [here][1].


## What is the purpose behind it?
Many users are not happy with their devices' ties to Google, so they try to cut
them back as far as possible. Not being bound to Google's Play Store is part of
that – but the choice of apps is quite restricted outside Playstore if you
don't want to replace one „evil“ with another (i.e. switching to another
„walled garden“ like the Amazon Store) and still have the comfort of updates.
The *IzzyOnDroid* repo wants to increase the number of apps available to this
group.


## Why another repository – hasn't F-Droid its own?
Indeed it has, and a very good one at that. But they also have very strong
[inclusion criteria][2] many apps cannot meet, or are not yet ready to meet
(most common examples include „blobs“ or dependencies on proprietary
frameworks). That's where the *IzzyOnDroid* repo jumps in: Most of those apps
are acceptable here, as the requirements are lower. So while e.g. preparing to
meet the stronger criteria „over there“ (it's always preferable to aim for
that), an app can be temporarily hosted here. Other apps also have their
permanent place at *IzzyOnDroid* because they won't ever get rid of those
dependencies – for example if those are a central part of the app's
functionality.


## How can I get my favorite app listed there?
This is what this project here at GitLab was set up for: use the issues to
propose new apps. Of course, first make sure the app you wish to add isn't
already there. Then, also check it meets the requirements below.


## What are the requirements an app must meet to be included with the repo?
The app …

* should be free (as in „free beer“ **and** as in „free speech“) and Open Source
* must be targeted at end users (so no libraries, proof-of-concept demos, etc.)
* its code must be freely accessible, preferably at [Github], [GitLab], [NotABug], [Codeberg] or a similar platform
* if the app processes sensitive data (e.g. health data, passwords), is intended to improve security/privacy, has root permissions, or is targeted at children, it must have no trackers at all
* it preferably has no proprietary components. While some of them might be acceptable, trackers (e.g. ads, analytics) are tolerated at best but only if there are no more than two such modules.
* that code repository should have a proper description so it gets clear what it is (if it's not, you must provide these details)
* the `.apk` file must be available from the project. Currently, *IzzyOnDroid* can work with files
  * attached to GitLab `tags/`
  * attached to Github `releases/`
  * attached to Gitea `releases/` (e.g. at [Codeberg])
  * uploaded inside a Github/GitLab repository as „part of the code“
  * fixed URLs (like `https://example.com/app-latest.apk`) which can be redirects, but the web server must provide a `Last Modified` header

Running on private ressources (no funding), *IzzyOnDroid* usually reserves up
to 30 megabytes per app (exceptions are being made for larger apps, so this is
considered as rule-of-thumb). That's at the same time the upper size limit for
single `.apk` files. If multiple files can fit in this limit, the repo holds up
to 3 versions. Also, certain categories of apps are not accepted – e.g. games.
Exceptions are made, but not often.


## Are there any app categories which are not acceptable to this repo?
* Games are unlikely to be accepted (though exceptions are possible, e.g. for educational games)
* Apps promoting violence, hate, harassment, racism and similar topics will definitely be rejected.
* „Explicit content“ is not welcome here – mainly for the reason that all age groups incl. „minors“ should have access to this repo.
* Apps which are loaded with trackers (Analytics, Ads, etc.) are no good, as they pose dangers to the user's privacy.
* Also for the reason of tracking, apps for Facebook, WhatsApp & Co won't be accepted.

This list is not complete (I might add to it if need arises), but should give you a raw idea.


## If my app is available here, do you have a badge I can use to link to it?
Yes, indeed I have – for the purpose of linking to my repo, you can use [this PNG](assets/IzzyOnDroid.png), available in the `assets/` directory:

<center><img src="assets/IzzyOnDroid.png" width="170"></center>


## I'm just a simple, not tech-savy user, how can I support you?
True, there's much time I spend on my open source projects, and server costs have
to be considered as well. So if you're just a happy user looking how you can give
some „backing“ – you're very welcome to do so spending some mBTC to my address
[1K7i1VJYjRgjdVzaMfK2XRxLsyhjSFXPnC](bitcoin:1K7i1VJYjRgjdVzaMfK2XRxLsyhjSFXPnC).
For other alternatives like SEPA payment, please find detailed [explanations at
IzzyOnDroid][3].

Apart from that, support is always welcomed with

* improving app descriptions
* providing missing screenshots
* reporting missing/wrong URLs (e.g. if an app's repo moved, or a website/changelog was added)
* finding and reporting good apps matching above criteria so they can be added
* reporting „violating” apps as well as such that no longer work so they can be removed

If you’re going on a hunt to report a bulk, please open one issue per app so it
will be easier to process. But even if you by accident stumble on a little thing
to be improved, your report will be welcome!


[IzzyOnDroid]: https://android.izzysoft.de/
[Github]: https://github.com/
[GitLab]: https://gitlab.com/
[NotABug]: https://notabug.org/
[Codeberg]: https://codeberg.org/
[1]: https://apt.izzysoft.de/fdroid/index/info
[2]: https://f-droid.org/wiki/page/Inclusion_Policy "F-Droid Inclusion Policy"
[3]: https://android.izzysoft.de/help?topic=support_us "Say thanks to IzzyOnDroid"
