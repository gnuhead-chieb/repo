This directory holds (some of) the libraries used with the [IzzyOnDroid
Repo](https://apt.izzysoft.de/fdroid). It will be complemented from time to
time, whenever I find the time to do so (mainly some reorganisation and adding
missing documentation to make it understandable for those not having written
the code).

If not specified otherwise, all code is protected by the GPL version 2.0.

## Library Scanner
As I don't compile apps from their resp. sources myself, I want (you to) at
least know what's inside the APKs. For this, the `*.apk` files are scanned
by different means:

* **[VirusTotal](https://virustotal.com/):** multiple malware scanner are run
  against the `*.apk` files there to see if they contain a threat. Results are
  indicated by „shield icons“, and are linked to the full result set at
  VirusTotal. For details on this, [see here](https://apt.izzysoft.de/fdroid/index/info#vt).
  The PHP library used for this can be [found at Codeberg.org](https://codeberg.org/izzy/php-virustotal),
  so I won't duplicate it here.
* **Local Library Scan:** APK files are reverse-engineered using
  [ApkTool](https://github.com/iBotPeaches/Apktool) and their paths are analyzed
  for what libraries have been used. This is a pure static scan comparable to
  the one performed by [Exodus Privacy](https://reports.exodus-privacy.eu.org/);
  it only tells us what libraries are contained in the `*.apk`, but not whether
  they are actually addressed by the app and used at runtime.  
  I've started this around the year 2016 and based it on pkumza's
  [LibRadar](https://github.com/pkumza/LibRadar/tree/V1) (yes, it still uses the
  V1 branch as the more recent master requires quite huge library definitions
  to be downloaded and kept up-to-date). As those library definitions are no
  longer updated, I've complemented them by my own. These you will find here
  along with the code I use to process the APKs.

If you want to use the library scanner stand-alone, take a look at my article
[Identify modules in apps](https://android.izzysoft.de/articles/named/app-modules-2).



## Files
### LibRadar
* `libradar.class.php`: the PHP wrapper around LibRadar and its complements.
  The code is documented using „PHPDoc“, including short instructions on its usage.
* `libradar.txt`, `libradar_wild.txt`: files complementing LibRadar's own library
  definitions. Structure etc. is explained inside `libradar.class.php`.
* `libsmali.txt`: my own library definitions, run against the Smali code left by
  LibRadar when it has extracted it via ApkTool. This is done after the results
  from LibRadar have been processed, and merged with those.


### My own library definitions
These are also shared with *Exodus Privacy* whenever I've found something not yet
available with their definitions. But they hold not only tracking stuff, but also
„general development libraries“ (and frameworks) like Flutter, Material Dialogs,
Color Pickers etc.

* `libsmali.txt`: see above.
* `libinfo.txt`: informational details to show to the user, like descriptions of
  the libraries, but also AntiFeatures triggered by a library (e.g. Ads, non-free
  dependencies).
* `libs_verificator.php`: a PHP script to verify the aforementioned two definition
  files are syntactically correct and have all mandatory fields set.


### the Apk-Checker
* `apkcheck_db_struct.sql`: database structure. I use SQLite for this (easy sync
  from the "job machine" to the "web server"), but these could be adopted for
  other DBs without much hazzle
* `apkcheck_db.class.php`: class dealing with the database. The required
  SQLite3 base class (or the one for MySQL/MariaDB if you prefer) can be obtained
  e.g. from [phpVideoPro](https://github.com/IzzySoft/phpVideoPro)
* `apkcheck.class.php`: the wrapper around VirusTotal and libradar classes
  dealing with checking `*.apk` files for malware and libraries. This is the
  only one directly addressed for this process.
* `apkscan.ini`: configuration
* `../bin/apkScan.php`: the main script – that's the one to run!
* `../bin/checkAntis.php`: cross-check each app's latest package against our
  scanner result library for possibly missing AntiFeatures (e.g. dragged in by
  a new library dependency)
* `../bin/cleanIcons`: script to remove icons of deleted apps (fdroidserver simply
  ignores them)
* `../bin/getInfected.sql`: controls what findings are reported when running `apkScan.php`
* `../bin/getInfectedChecker`: checks the exception rules in `getInfected.sql` and
  automatically removes lines for APKs that no longer exist
* `../bin/getRelease.php`: the core CLI script to check for updates and retrieve them
* `../bin/locallibs.php`: cross-check Smali from apps against our library definitions
  for new candidates
* `../bin/rescan.php`: to rescan an APK file (eg. after library definitions were
  updated) and/or schedule a rescan with VirusTotal
* `../bin/scanapk.php`: to manually check an APK file for libraries. Will output a list
  of libraries detected, as well as a second list of libraries having AntiFeatures (if
  such were found in the app)


### Other files
* `ApkUpdater.class.inc`: the core library taking care to check for and pull
  updated APKs from various sources, extending `upstream.class.php`
* `antifeatures.json`: descriptions of the AntiFeatures you might already know from
  [F-Droid](https://f-droid.org/). This is used [on the website](https://apt.izzysoft.de/fdroid)
  with the app descriptions.
* `antifeatures_spans.json`: corresponding JSON for the symbols indicating
  AntiFeatures in lists (HTML SPANs)
* `class.logging.php`: simple logging class used by CLI executions e.g. of the Apk-Checker
* `upstream.class.php`: dealing with „upstream APIs“, like Github/GitLab/Gitea, to
  obtain information on Releases, Files etc. Also uses `apkscan.ini` for configuration
  (see Apk-Checker above).
* `yaml/`: YAML parser from the Symfony project. This intentionally is an older, light-weight
  version not drawing in thousands of dependencies, and used to parse metadata files of
  our apps (for things not contained in the F-Droid index, especially the specific details
  maintained in `MaintainerNotes`)


### Files I won't publish here
… because they can be found in their own repos:

* `fdroid.class.php`: the class I wrote to get app details from F-Droid's XML and
  JSON indexes can [be found here](https://gitlab.com/fdroid/php-fdroid)
* `virustotal.class.php`: the class I wrote to have VirosTotal scan the APK files.
   You can [find it at Codeberg.Org](https://codeberg.org/izzy/php-virustotal)
* [LibRadar](https://github.com/pkumza/LibRadar/tree/V1) & [ApkTool](https://github.com/iBotPeaches/Apktool)
* Michel Fortin's [PHP Markdown Extra](https://michelf.ca/projects/php-markdown/extra/) (used for cleanup/formatting of some Fastlane `full_description.txt`)
