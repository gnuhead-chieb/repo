<?php
/**
 * Make sure library files contain only valid JSON, each record contains all
 * mandatory fields, and each record uses a valid type (as defined by LibRadar)
 */
$files = ['libsmali.txt','libinfo.txt'];
$fields = [
  'libsmali.txt' => [ 'id','path','name','type','perms','url' ],
  'libinfo.txt' => [ 'id','emphasize','details','mwid','anti' ]
];
$types = ["UI Component", "Development Framework", "Utility", "Development Aid",
          "Social Network", "Advertisement", "App Market", "Mobile Analytics",
          "Payment", "Game Engine", "Map"
];
$errors = 0;

foreach ( $files as $file ) {
  $line = 0;
  $defs = file ($file);
  foreach ( $defs as $def ) {
    ++$line;
    if ( !$res = json_decode($def) ) {
      echo "Invalid definition in ${file} at line ${line}:\n$def\n";
      ++$errors;
      continue;
    }
    foreach ( $fields[$file] as $field ) {
      if ( !property_exists($res,$field) ) {
        echo "${file} line ${line} lacks property '$field'\n";
        ++$errors;
      }
    }
    if ( empty($res->id) ) {
      echo "${file} line ${line}: 'id' must not be empty!\n";
      ++$errors;
    }
    if ( $file == 'libsmali.txt' ) {
      foreach(['path','name','type'] as $name) {
        if ( empty($res->{$name}) && $res->id != '/com/module/id' ) {
          echo "${file} line ${line}: '$name' must not be empty!\n";
          ++$errors;
        }
      }
      if ( !in_array($res->type,$types) && $res->id != '/com/module/id' ) {
        echo "${file} line ${line}: invalid type '".$res->type."' for '".$res->id."'\n";
        ++$errors;
      }
      if ( !preg_match('!^https?://!',$res->url) && !empty($res->url) ) {
        echo "${file} line ${line}: invalid URL '".$res->url."' for '".$res->id."'\n";
        ++$errors;
      }
    }
  }
}
echo "$errors error(s) found in library definitions.\n";
if ( $errors > 0 ) exit(1);
?>