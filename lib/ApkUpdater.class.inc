<?php
require_once('upstream.class.php');
require_once('apkcheck.class.php');
require_once('fdroid.class.php');
require_once('yaml/lib/sfYamlParser.php');
require_once 'Michelf/MarkdownExtra.inc.php'; // to format some fastlane fulldescs

/** Retrieving APK updates from various sources and processing the files.
 * @class ApkUpdater
 */
class ApkUpdater extends upstream {

  protected $repoDir = '';      // base directory of our repository
  protected $repoHost;          // hostname (evaluated from index, needed for URL sanitizing)
  protected $fdroidserver = ''; // fdroid server executable
  protected $apkcheck;          // apkcheck instance
  public    $repoData = [];     // app data loaded from our repo index by getRepoData()


// ----------------------------------------------------------------------------
  /** Setting up the class instance
   * @constructor ApkUpdater
   * @param          string inifile  config file to use
   * @param optional object logger   logger instance (default: null = use "null-logger" aka NOLOG)
   */
  function __construct($inifile,$logger=null) {
    parent::__construct($inifile,$logger);
    $this->repoDir = dirname($this->conf['general']['apkdir']);
    $this->fdroidserver = $this->conf['general']['fdroidserver'];
    // init APK checker
    $this->apkcheck = new apkcheck($this->conf['general']['apkdir'],$this->conf['general']['database']);
    $this->apkcheck->initRadar($this->conf['libradar']['radarDir'],$this->conf['libradar']['libsFile'],$this->conf['libradar']['libsWildFile'],$this->conf['libradar']['libsSmaliFile'],'echo');
    $this->apkcheck->initVT($this->conf['virustotal']['apikey']);
    // load repo data
    $this->getRepoData();
  }


// ----------------------------------------------------------------------------
  /** Collect data from our local repository
   * @class ApkUpdater
   * @method getRepoData
   * @return array[0..n] of array[id,url,ver,method]; id=package_name, url=release-page, ver=local version, method=update-method
   */
  protected function getRepoData() {
    $fdroid = new fdroid('../repo/index-v1.json',0);
    $this->repoHost = parse_url($fdroid->getMeta()->url,PHP_URL_HOST);
    $apps = $fdroid->getAppList(0,99999); // from 0 to ∞

    $list = [];
    foreach($apps as $app) {
      $id = $app->id;
      $src = $app->source;
      if ( is_array($app->package) ) {
        $pack = $app->package[0]; $vc = $pack->versioncode;
        foreach($app->package as $packn) {
          if ( $vc < $packn->versioncode ) { // in some rare cases, order is wrong
            $pack = $packn; $vc = $packn->versioncode;
          }
        }
      } else $pack = $app->package;
      $ver = $pack->version; $vercode = $pack->versioncode;
      $filetime = $app->lastupdated; // independent from the file timestamp, we want only files added *after* our last update
      $filetimestamp = $pack->added_ts;

      // parse Metadata file:
      if ( file_exists($this->repoDir.'/metadata/'.$id.'.yml') ) { // YAML metadata
        $parser = new sfYamlParser();
        $yaml = $parser->parse( file_get_contents($this->repoDir.'/metadata/'.$id.'.yml') );
        ( isset($yaml['AutoUpdateMode']) ) ? $aum = $yaml['AutoUpdateMode'] : $aum = '';
        ( isset($yaml['UpdateCheckMode']) ) ? $ucm = $yaml['UpdateCheckMode'] : $ucm = '';
        // UCM Regex pattern: ^(Tags|Tags .+|RepoManifest|RepoManifest/.+|RepoTrunk|HTTP|Static|None)$ (we only use Tags|Static|None currently)
        ( isset($yaml['MaintainerNotes']['GHSkipPre']) ) ? $skipPre = $yaml['MaintainerNotes']['GHSkipPre'] : $skipPre = 0;
        ( isset($yaml['MaintainerNotes']['ApkMatch']) ) ? $apkMatch = $yaml['MaintainerNotes']['ApkMatch'] : $apkMatch = '';
        ( isset($yaml['MaintainerNotes']['ApkIgnore']) ) ? $apkIgnore = $yaml['MaintainerNotes']['ApkIgnore'] : $apkIgnore = '';
        ( isset($yaml['MaintainerNotes']['ApkUrl']) ) ? $apkUrl = $yaml['MaintainerNotes']['ApkUrl'] : $apkUrl = '';
        ( isset($yaml['MaintainerNotes']['GHDownloadDir']) ) ? $_GHDownloadDir = $yaml['MaintainerNotes']['GHDownloadDir'] : $_GHDownloadDir = '';
        ( isset($yaml['MaintainerNotes']['Method']) ) ? $method = $yaml['MaintainerNotes']['Method'] : $method = '';
        ( isset($yaml['MaintainerNotes']['Fastlane']) ) ? $fastlane = $yaml['MaintainerNotes']['Fastlane'] : $fastlane = '';
        ( isset($yaml['MaintainerNotes']['FastlaneFeatures']) ) ? $fastlaneFeatures = explode(',',$yaml['MaintainerNotes']['FastlaneFeatures']) : $fastlaneFeatures = '';
        ( isset($yaml['MaintainerNotes']['FastlaneService']) ) ? $fastlaneService = $yaml['MaintainerNotes']['FastlaneService'] : $fastlaneService = $method;
        ( isset($yaml['MaintainerNotes']['ManualHint']) ) ? $manualHint = $yaml['MaintainerNotes']['ManualHint'] : $manualHint = '';
        ( isset($yaml['VercodeOperation']) ) ? $vercodeOp = $yaml['VercodeOperation'] : $vercodeOp = '';
      } else {
        $this->log->error("No metadata file (yml) for '$id', skipping");
        continue;
      }

      if ( $method == 'gitlab-tags' ) $url = $src . '/tags';
      else $url = $src . '/releases';
      if ( (empty($method) || $method=='github-repo') && !empty($_GHDownloadDir) ) {
        $method = 'github-repo';
        $url    = $_GHDownloadDir;
      } elseif ( $method=='gitlab-repo' ) {
        $url    = $_GHDownloadDir;
      }
      if ( $url=='/releases' && preg_match('!^github!',$method) ) {
        $this->log->error("No release URL for '$id', skipping");
        continue;
      }
      if ( empty($method) && !empty($apkUrl) ) $method = 'direct-link';

      $list[$id] = ['id'=>$id,'src'=>$src,'url'=>$url,'ver'=>$ver,'vercode'=>$vercode,'method'=>$method,'skipPre'=>$skipPre,'filetime'=>$filetime,'filetimestamp'=>$filetimestamp,
                    'apkmatch'=>$apkMatch,'apkignore'=>$apkIgnore,'apkurl'=>$apkUrl,'manualhint'=>$manualHint,'aum'=>$aum,'ucm'=>$ucm,'fastlane'=>$fastlane,
                    'fastlaneFeatures'=>$fastlaneFeatures,'fastlaneService'=>$fastlaneService,'vercodeOp'=>$vercodeOp];
    }

    ksort($list);
    $this->repoData = $list;
  }

#####################################################[ General remote stuff ]###
  /** Deal with downloading APKs
   * @class ApkUpdater
   * @method processDownload
   * @param string fn       filename (with full path) to use for storing the file on our end
   * @param string url      URL to download
   * @param string pkgname  package name of the app (for apkcheck)
   * @return mixed vercode  versionCode of the downloaded APK or FALSE if download failed
   */
  public function processDownload($fn,$url,$pkgname) {
    if ( file_exists($fn) && filesize($fn) > 0 ) {
      $this->log->warn("$fn already here, not downloading." ); // rare cases
      return;
    }
    $msg = "- Grabbing update for ${pkgname}: ";
    passthru("wget -q -O $fn '$url'", $rc);
    if ( file_exists($fn) && filesize($fn) == 0 ) {
      $this->log->warn($msg."Fail (got 0 byte file from '$url')");
      unlink($fn);
      return FALSE;
    } elseif ( ! file_exists($fn) ) {
      $this->log->warn($msg."Fail (no data from'$url')");
      return FALSE;
    }
    if ( $rc == 0 ) { // to keep the file date
      $timestamp = exec("unzip -v $fn |awk '{print \$5\" \"\$6}' |sort|egrep '^[0-9]'|tail -n 1");
      if (strtotime($timestamp) > strtotime('2011-01-01')) touch($fn,strtotime($timestamp)); // protect against those 1980-01-01
      $this->log->info($msg."OK");
      $vercode = shell_exec("aapt d badging $fn | head -n 1");
      $vercode = trim(preg_replace('!^.+versionCode=.(\d+).+$!','\1',$vercode));
      $filename = $this->repoDir."/repo/${pkgname}_${vercode}.apk";
      if ( $fn != $filename ) { // if fetched via versionCode, we cannot rename it to itself
        if ( file_exists($filename) ) {
          passthru("diff $filename $fn", $diff);
          if ($diff) {
            $this->log->warn("${filename} already exists, quarantining before replacing it with update '$fn'");
            rename($filename,$this->repoDir."/quarantine/".basename($filename).'.'.filectime($filename));
          } else {
            $this->log->warn("${filename} already exists and is identical to the one we downloaded; ignoring it");
            unlink($fn);
            return false;
          }
        }
        rename($fn,$filename);
      }
      $this->log->info("- Checking '$filename' for libraries and malware …");
      $this->apkcheck->newFile($pkgname,basename($filename),$url);
      $this->apkcheck->vtFile(basename($filename));
      return $vercode;
    } else {
      $this->log->warn($msg."Fail (RC $rc)");
      return FALSE;
    }
  }


// ----------------------------------------------------------------------------
  function fromMD($text) {
    $parser = new Michelf\MarkdownExtra();
    $parser->code_attr_on_pre = true;
    $text = $parser->transform($text);
    return trim( $this->sanitizeHTML($text,$this->repoHost) );
  }

  /** Get Fastlane data
   * @class ApkUpdater
   * @method getFastlane
   * @param array app app packagedata
   */
   function getFastlane($app,$vercode='') {
     if ( empty($app['fastlane']) ) return;
     if (empty($vercode)) $vercode = $app['vercode']; // previous version
     $url = preg_replace('!/(blob|tree|src)/!','/raw/',$app['fastlane']);
     if ( preg_match('!/[a-z]{2}(-[A-Z]{2})?$!', $url) ) { // ends in locale (xx-XX or just xx) => find changelog here
       $this->log->debug($app['id'] . ": checking Fastlane for per-release changelogs");
       if ( empty($app['vercodeOp']) ) $remver = $vercode;
       else $remver = eval(str_replace('%c',"return $vercode",$app['vercodeOp']).';');
       if ( $changelog = @file_get_contents("${url}/changelogs/${remver}.txt") ) {
         $target = $this->repoDir . '/metadata/' . $app['id'] . '/en-US/changelogs';
         if ( !file_exists($target) ) mkdir($target,0755,true);
         if ( !file_exists($target) ) {
           $this->log->error("Could not create '$target'!");
           return;
         }
         $target .= '/' . $vercode . '.txt';
         if ( !file_exists($target) ) { file_put_contents($target,$changelog); }
         $this->log->debug($app['id'] . ": fetched '${url}/changelogs/${remver}.txt'");
       } else {
         $err = substr($http_response_header[0],9,3);
         $this->log->warn($app['id'] . ": Fastlane: got $err trying to retrieve ${url}/changelogs/${remver}.txt");
       }
       $url = dirname($url); // strip locale for following operations
     }
     // retrieve additional data from Fastlane if newer than ours:
     if ( !empty($app['fastlaneFeatures']) ) {
       $apath = parse_url($app['url']);
       $path = explode('/',$apath['path']);
       $host = $apath['host']; $owner = $path[1]; $repo = $path[2];
       $fpath = parse_url(preg_replace('!/[^/]+/[^/]+/raw/[^/]+/!','/',$url),PHP_URL_PATH); // strip '/$owner/$repo/raw/$branch'
       switch( preg_replace('!^(.+?)-.*!','$1',$app['fastlaneService']) ) {
         case 'github':
           $service = 'github';
           $spec = ['host'=>$host,'owner'=>$owner,'repo'=>$repo,'path'=>$fpath, 'appId'=>$app['id']];
           break;
         case 'gitlab':
           $service = 'gitlab';
           $spec = ['host'=>$host,'owner'=>$owner,'repo'=>$repo,'path'=>$fpath, 'appId'=>$app['id']];
           break;
         case 'codeberg':
           $service = 'codeberg';
           $spec = ['host'=>$host,'owner'=>$owner,'repo'=>$repo,'path'=>$fpath, 'appId'=>$app['id']];
           break;
         default:   // (currently) unsupported, should not come here
           $this->log->error($app['id'].": FastlaneFeatures not supported with method '".$app['fastlaneService']."'");
           return;
           break;
       }
       $this->log->debug($app['id'].": calling 'getFastlaneMeta($service,[host:$host,owner:$owner,repo:$repo,path:$fpath])'");
       $this->log->debug($app['id'].": FastlaneFeatures ".implode(',',$app['fastlaneFeatures']));
       $fastlane = $this->getFastlaneMeta($service,$spec);
       foreach ( $fastlane as $loc => $meta ) {
         $this->log->debug($app['id'].": checking locale '$loc'");
         if ( isset($meta['text']) ) {
           $targetRoot = $this->repoDir . '/metadata/' . $app['id'];
           foreach ( $meta['text'] as $item ) {
             if ( empty($item['ts']) ) continue;
             $name = $item['name'];
             if ( file_exists("${targetRoot}/$loc/${name}") && filemtime("${targetRoot}/$loc/${name}") >= $item['ts'] ) continue;
             if ( $name=='title.txt' && in_array('title',$app['fastlaneFeatures'])
               || $name=='video.txt' && in_array('video',$app['fastlaneFeatures'])
               || $name=='short_description.txt' && in_array('shortdesc',$app['fastlaneFeatures'])
               || $name=='full_description.txt' && in_array('fulldesc',$app['fastlaneFeatures']) ) { // fulldesc,shortdesc,title: take as-is
               if ( $content = @file_get_contents($item['url']) ) {
                 $this->log->debug($app['id'].": updating '${targetRoot}/$loc/$name'");
                 if ( !file_exists("${targetRoot}/$loc") ) mkdir("${targetRoot}/$loc",0755,true);
                 if ( $name=='full_description.txt' ) file_put_contents("${targetRoot}/$loc/${name}",$this->sanitizeHTML($content,$this->repoHost));
                 else file_put_contents("${targetRoot}/$loc/${name}",strip_tags($content));
               } else {
                 $this->log->warn($app['id'].": failed retrieving '".$item['url']."'");
               }
             } elseif ( $name=='full_description.txt' && in_array('fulldescNL',$app['fastlaneFeatures']) ) { // fulldescNL: nl2br
               if ( $content = @file_get_contents($item['url']) ) {
                 $this->log->debug($app['id'].": replacing '${targetRoot}/$loc/$name' with NL2BR");
                 if ( !file_exists("${targetRoot}/$loc") ) mkdir("${targetRoot}/$loc",0755,true);
                 file_put_contents("${targetRoot}/$loc/${name}",$this->sanitizeHTML(nl2br($content),$this->repoHost));
               } else {
                 $this->log->warn($app['id'].": failed retrieving '".$item['url']."'");
               }
             } elseif ( $name=='full_description.txt' && in_array('fulldescMD',$app['fastlaneFeatures']) ) { // fulldescMD: fromMD
               if ( $content = @file_get_contents($item['url']) ) {
                 $this->log->debug($app['id'].": replacing '${targetRoot}/$loc/$name' with fromMD");
                 if ( !file_exists("${targetRoot}/$loc") ) mkdir("${targetRoot}/$loc",0755,true);
                 file_put_contents("${targetRoot}/$loc/${name}",$this->fromMD($content));
               } else {
                 $this->log->warn($app['id'].": failed retrieving '".$item['url']."'");
               }
             }
           }
           if ( in_array('changelogs',$app['fastlaneFeatures']) ) foreach ( $meta['changelogs'] as $item ) {
             if ( empty($item['ts']) ) continue;
             $name = $item['name'];
             if ( file_exists("${targetRoot}/$loc/changelogs/${name}") && filemtime("${targetRoot}/$loc/changelogs/${name}") >= $item['ts'] ) continue;
             if ( $content = @file_get_contents($item['url']) ) {
               $this->log->debug($app['id'].": updating '${targetRoot}/$loc/changelogs/$name'");
               if ( !file_exists("${targetRoot}/$loc/changelogs") ) mkdir("${targetRoot}/$loc/changelogs",0755,true);
               file_put_contents("${targetRoot}/$loc/changelogs/${name}",$content);
             } else {
               $this->log->warn($app['id'].": failed retrieving '".$item['url']."'");
             }
           }
         }
         if ( isset($meta['images']) ) {
           $targetRoot = $this->repoDir .'/repo/' . $app['id'];
           foreach ( $meta['images'] as $item ) {
             if ( empty($item['ts']) ) continue;
             $name = $item['name'];
             if ( !preg_match('!\.(jpe?g|png)$!i',$item['name']) ) {
               $this->log->debug($app['id'].": only PNG and JPEG are supported for graphics, skipping '".$item['name']."'");
               continue;
             }
             if ( file_exists("${targetRoot}/$loc/${name}") && filemtime("${targetRoot}/$loc/${name}") >= $item['ts'] ) continue;
             if ( $name=='icon.png' && in_array('icon',$app['fastlaneFeatures']) ) { // icon: resize 48x48 + optipng
               $this->log->debug($app['id'].": updating '${targetRoot}/$loc/$name'");
               if ( !file_exists("${targetRoot}/$loc") ) mkdir("${targetRoot}/$loc",0755,true);
               @imagepng( @imagescale( @imagecreatefromstring(file_get_contents($item['url'])),48,48 ),"${targetRoot}/$loc/$name" );
               passthru("optipng -quiet -o 2 ${targetRoot}/$loc/$name");
             }
             // featureGraphic: just optipng (& resize 512x250); note this also can be featureGraphic.jpg
             elseif ( ($name=='featureGraphic.png' || $name=='featureGraphic.jpg') && in_array('featureGraphic',$app['fastlaneFeatures']) ) {
               if ( $im = imagescale( imagecreatefromstring(@file_get_contents($item['url'])),512 ) ) {
                 $this->log->debug($app['id'].": updating '${targetRoot}/$loc/$name'");
                 if ( !file_exists("${targetRoot}/$loc") ) mkdir("${targetRoot}/$loc",0755,true);
                 if ($name=='featureGraphic.png') { imagepng($im,"${targetRoot}/$loc/$name"); passthru("optipng -quiet -o 2 ${targetRoot}/$loc/$name"); }
                 else imagejpeg($im,"${targetRoot}/$loc/$name"); // uses quality ~75
                 imagedestroy($im);
               }
             }
             // featureGraphicJPG: as before, but convert to JPG plus cleanup/compress
             elseif ( $name=='featureGraphic.png' && in_array('featureGraphicJPG',$app['fastlaneFeatures']) ) {
               $this->log->debug($app['id'].": updating '${targetRoot}/$loc/featureGraphic.jpg' from $name'");
               if ( !file_exists("${targetRoot}/$loc") ) mkdir("${targetRoot}/$loc",0755,true);
               @imagejpeg( @imagescale( @imagecreatefromstring(file_get_contents($item['url'])),512 ),"${targetRoot}/$loc/featureGraphic.jpg" );
             }
           }
         }
         if ( isset($meta['phoneScreenshots']) ) {
           $targetRoot = $this->repoDir .'/repo/' . $app['id'];
           $newScreenshots = false;
           foreach ( $meta['phoneScreenshots'] as $item ) {
             if ( empty($item['ts']) ) continue;
             $name = $item['name'];
             if ( !preg_match('!\.(jpe?g|png)$!i',$item['name']) ) {
               $this->log->debug($app['id'].": only PNG and JPEG are supported for graphics, skipping '".$item['name']."'");
               continue;
             }
             if ( file_exists("${targetRoot}/$loc/phoneScreenshots/${name}") && filemtime("${targetRoot}/$loc/phoneScreenshots/${name}") >= $item['ts'] ) continue;
             if ( in_array('screenshots',$app['fastlaneFeatures']) ) { // screenshots: phoneScreenshots w/ resize and optipng (no reformat)
               if ( $im = imagecreatefromstring(@file_get_contents($item['url'])) ) {
                 $this->log->debug($app['id'].": updating '${targetRoot}/$loc/phoneScreenshots/$name'");
                 if ( !file_exists("${targetRoot}/$loc/phoneScreenshots") ) mkdir("${targetRoot}/$loc/phoneScreenshots",0755,true);
                 $sx = imagesx($im); $sy = imagesy($im);
                 if ( $sx > 350 && $sy > 350 ) { // resize
                   if ( $sx < $sy ) { $im2 = imagescale($im,350); } // portrait
                   else $im2 = imagescale($im,350*imagesx($im)/imagesy($im),350); // landscape
                   if ( strtolower(pathinfo($item['path'],PATHINFO_EXTENSION))=='png' ) {
                     imagepng($im2,"${targetRoot}/$loc/phoneScreenshots/${name}");
                     passthru("optipng -quiet -o 2 ${targetRoot}/$loc/phoneScreenshots/${name}");
                   } else imagejpeg($im2,"${targetRoot}/$loc/phoneScreenshots/${name}");
                   imagedestroy($im2);
                 } else {
                   if ( strtolower(pathinfo($item['path'],PATHINFO_EXTENSION))=='png' ) {
                     imagepng($im,"${targetRoot}/$loc/phoneScreenshots/${name}");
                     passthru("optipng -quiet -o 2 ${targetRoot}/$loc/phoneScreenshots/${name}");
                   } else imagejpeg($im,"${targetRoot}/$loc/phoneScreenshots/${name}");
                 }
                 imagedestroy($im);
                 $newScreenshots = true;
               } else {
                 $this->log->warn($app['id'].": failed retrieving '".$item['url']."'");
               }
             } elseif (in_array('screenshotsJPG',$app['fastlaneFeatures']) ) { // screenshotsJPG: as before, but convert to JPG plus cleanup/compress
               if ( $im = @imagecreatefromstring(@file_get_contents($item['url'])) ) {
                 $this->log->debug($app['id'].": updating '${targetRoot}/$loc/phoneScreenshots/$name' as JPG");
                 if ( !file_exists("${targetRoot}/$loc/phoneScreenshots") ) mkdir("${targetRoot}/$loc/phoneScreenshots",0755,true);
                 $sx = imagesx($im); $sy = imagesy($im);
                 if ( $sx > 350 && $sy > 350 ) { // resize
                   if ( $sx < $sy ) { $im2 = imagescale($im,350); } // portrait
                   else $im2 = imagescale($im,350*imagesx($im)/imagesy($im),350); // landscape
                   imagejpeg($im2,"${targetRoot}/$loc/phoneScreenshots/".pathinfo($name,PATHINFO_FILENAME).".jpg");
                   imagedestroy($im2);
                 } else { // no resize
                   imagejpeg($im,"${targetRoot}/$loc/phoneScreenshots/".pathinfo($name,PATHINFO_FILENAME).".jpg");
                 }
                 imagedestroy($im);
                 $newScreenshots = true;
               } else {
                 $this->log->warn($app['id'].": failed retrieving '".$item['url']."'");
               }
             }
           }
           if ( $newScreenshots ) { // cross-check for removed screenshots
             $this->log->debug($app['id'].": cross-checking for obsolete screenshots");
             $imgs = $fimgs = [];
             foreach ( $meta['phoneScreenshots'] as $item ) $imgs[] = pathinfo($item['path'],PATHINFO_FILENAME);
             $this->log->debug($app['id'].": screenshots in Fastlane: ".implode(',',$imgs));
             foreach(glob("${targetRoot}/$loc/phoneScreenshots/*") as $img) {
               $fimgs[] = pathinfo($img,PATHINFO_FILENAME);
               if ( !in_array(pathinfo($img,PATHINFO_FILENAME),$imgs) ) {
                 $this->log->debug($app['id'].": removing $img");
                 unlink($img);
               }
             }
             $this->log->debug($app['id'].": local screenshots checked: ".implode(',',$fimgs));
           }
         }
       }
     }
   }


// ----------------------------------------------------------------------------
  /** Compare versions
   * @class ApkUpdater
   * @method newerVersion
   * @param string oldver   version we already have
   * @param string newver   version available remotely
   * @return int update     1 if newver > oldver, 0 otherwise
   */
  function newerVersion($oldver,$newver) {
    $oldver = preg_replace('![^\d\.\-]!','',$oldver);
    $newver = preg_replace('![^\d\.\-]!','',$newver);
    if ( empty($newver) ) return 0; // either there is no new version, or we cannot find it
    $old = explode('.',$oldver); if (strpos($old[count($old)-1],'-')) { $f=explode('-',$old[count($old)-1]); $old[count($old)-1] = $f[0]; $old[] = $f[1]; }
    $new = explode('.',$newver); if (strpos($new[count($new)-1],'-')) { $f=explode('-',$new[count($new)-1]); $new[count($new)-1] = $f[0]; $new[] = $f[1]; }
    $len = max(count($old),count($new));
    for ($i=0;$i<$len;++$i) {
      if ( isset($old[$i]) && isset($new[$i]) ) { if ( $old[$i] < $new[$i] ) return 1; elseif ( $old[$i] > $new[$i] ) return 0; } // yes, older version published later
      if ( !isset($old[$i]) && isset($new[$i]) )                       return 1; // new level added
      //if ( isset($old[$i]) && !isset($new[$i]) )                       return 1; // level dropped (usually beta -> release); doesn't cover 1.2.3-beta1 fails 1.2.2 -> 1.2
    }
    return 0;
  }


// ----------------------------------------------------------------------------
  /** Check a package
   * @class ApkUpdater
   * @method checkPackage
   * @param          mixed  app         either str(packageName) or array(packageData)
   * @param optional string run_mode    all (for manual run of a single package),auto (default),build,epo,manual,gone
   * @return int updates 0: none, >0: yes
   */
  function checkPackage($app,$run_mode='auto') {
    if ( is_string($app) ) { // package name only
      if ( empty($this->repoData) ) $this->getRepoData();
      if ( !array_key_exists($app,$this->repoData) ) {
        $this->log->error('checkPackage: $app does not exist in this repo, skipping.');
        return false;
      }
      $app = $this->repoData[$app];
    }
    if ( empty($app) ) {
      $this->log->error('checkPackage: $app is empty, skipping.');
      return FALSE;
    } elseif ( !is_array($app) ) {
      $this->log->error('checkPackage: $app must be a string (package name) or array. Skipping this.');
      return FALSE;
    }
    $updates = 0;

    switch(strtolower($app['method'])) {
      case 'gitlab-tags':
          if (!($run_mode=='auto' && $app['ucm']=='Tags' || $run_mode=='all' || $run_mode=='static' && $app['ucm']=='Static')) break;
          $gh = $this->getGitLabLastRel($app);
          if ( !array_key_exists('rel',$gh) ) {
            $this->log->warn($app['id'].": Could not obtain release info, skipping.");
            return 0;
          }
          if ( $this->newerVersion($app['ver'],$gh['rel']) ) { // update available
            if ( empty($gh['file']) ) {
              $this->log->warn($app['id'].": ".$app['ver']."/".$gh['rel']."; this release has no .apk");
              break;
            }
            $this->log->info($app['id'].": ".$app['ver']."/".$gh['rel'].", GH ".$app['url'].": ".$gh['file']);
            if ( !preg_match('!\.apk$!',$gh['file']) ) { $this->log->info("- ".$app['id'].": invalid file name, doesn't look like an APK: '".$gh['file']."'"); break; }
            $fn = $this->repoDir.'/repo/'.preg_replace('!(.+)\.apk$!',$app['id'].'_'.$gh['rel'].'.apk',$gh['file']); // version-specific name
            if (!preg_match('!^https://!',$gh['file'])) $gh['file'] = 'https://'.parse_url($app['url'])['host'].$gh['file'];
            if ( $vercode = $this->processDownload($fn, $gh['file'], $app['id']) ) {
              ++$updates;
              $this->getFastlane($app,$vercode);
            }
          }
          else $this->log->info($app['id'].": ".$app['ver']." up-to-date.");
          break;
      case 'gitlab-repo':
          if (!(in_array($run_mode,['auto','repo']) && empty($app['ucm']) || $run_mode=='all' || $run_mode=='static' && $app['ucm']=='Static')) break;
          $files = $this->getGitlabRepoApks($app);
          if ( empty($files) ) $this->log->warn($app['id'].": no files found at ".$app['url']." (tempGlitch? use '-v' to find out)"); // there seem to be temporary glitches at GH
          elseif ( $app['filetimestamp'] < array_keys($files)[0] ) { // update available
            $this->log->info( $app['id'].": ".$app['filetimestamp']." (".$app['filetime'].") => ".array_keys($files)[0] );
            if ( empty($files[array_keys($files)[0]]) ) {
              $this->log->error($app['id'].": no files found to download (method: github-repo)");
              break;
            }
            $fn = $this->repoDir.'/repo/'.$app['id'].'_'.array_keys($files)[0].'.apk';
            if ( $vercode = $this->processDownload($fn, $files[array_keys($files)[0]], $app['id']) ) {
              ++$updates;
              $this->getFastlane($app,$vercode);
            }
          } else {
            $this->log->info( $app['id']." up-to-date." );
          }
          break;
      case 'codeberg-release':
          if (!($run_mode=='auto' && $app['ucm']=='Tags' || $run_mode=='all' || $run_mode=='static' && $app['ucm']=='Static')) break;
          $gh = $this->getCodebergLastRel($app);
          if ( !array_key_exists('rel',$gh) ) {
            $this->log->warn($app['id'].": Could not obtain release info, skipping.");
            return 0;
          }
          if ( $this->newerVersion($app['ver'],$gh['rel'] ) ) { // update available
            if ( empty($gh['file']) ) {
              $this->log->warn($app['id'].": ".$app['ver']."/".$gh['rel']."; this release has no .apk");
              break;
            }
            $this->log->info($app['id'].": ".$app['ver']."/".$gh['rel'].", GH ".$app['url'].": ".$gh['file']);
            if ( !preg_match('!\.apk$!',$gh['name']) ) { $this->log->info("- ".$app['id'].": invalid file name, doesn't look like an APK: '".$gh['name']."'"); break; }
            $fn = $this->repoDir.'/repo/'.$app['id'].'_'.$gh['rel'].'.apk'; // version-specific name
            if ( $vercode = $this->processDownload($fn, $gh['file'], $app['id']) ) {
              ++$updates;
              $this->getFastlane($app,$vercode);
            }
          }
          else $this->log->info($app['id'].": ".$app['ver']." up-to-date.");
          break;
      case 'github-release':
          if (!($run_mode=='auto' && $app['ucm']=='Tags' || $run_mode=='all' || $run_mode=='static' && $app['ucm']=='Static')) break;
          $gh = $this->getGithubLastRel($app);
          if ( !array_key_exists('rel',$gh) ) {
            $this->log->warn($app['id'].": Could not obtain release info, skipping.");
            return 0;
          }
          if ( strpos($app['aum'],'%c') !== false && strpos($app['aum'],'%v') === false ) $cv = $app['vercode'];
          else $cv = $app['ver'];
          if ( $this->newerVersion($cv,$gh['rel'] ) ) { // update available
            if ( empty($gh['file']) ) {
              $this->log->warn($app['id'].": ".$app['ver']."/".$gh['rel']."; this release has no .apk");
              break;
            }
            $this->log->info($app['id'].": ".$app['ver']."/".$gh['rel'].", GH ".$app['url'].": ".$gh['file']);
            if ( !preg_match('!\.apk$!',$gh['file']) ) { $this->log->info("- ".$app['id'].": invalid file name, doesn't look like an APK: '".$gh['file']."'"); break; }
            $fn = $this->repoDir.'/repo/'.preg_replace('!(.+)\.apk$!',$app['id'].'_'.$gh['rel'].'.apk',$gh['file']); // version-specific name
            if ( $vercode = $this->processDownload($fn, $gh['file'], $app['id']) ) {
              ++$updates;
              $this->getFastlane($app,$vercode);
            }
          }
          else $this->log->info($app['id'].": ".$app['ver']." up-to-date.");
          break;
      case 'github-repo':
          if (!(in_array($run_mode,['auto','repo']) && empty($app['ucm']) || $run_mode=='all' || $run_mode=='static' && $app['ucm']=='Static')) break;
          $files = $this->getGithubRepoApks($app);
          if ( empty($files) ) $this->log->warn($app['id'].": no files found at ".$app['url']." (tempGlitch? use '-v' to find out)"); // there seem to be temporary glitches at GH
          elseif ( $app['filetimestamp'] < array_keys($files)[0] ) { // update available
            $this->log->info( $app['id'].": ".$app['filetimestamp']." (".$app['filetime'].") => ".array_keys($files)[0] );
            if ( empty($files[array_keys($files)[0]]) ) {
              $this->log->error($app['id'].": no files found to download (method: github-repo)");
              break;
            }
            $fn = $this->repoDir.'/repo/'.$app['id'].'_'.array_keys($files)[0].'.apk';
            if ( $vercode = $this->processDownload($fn, $files[array_keys($files)[0]], $app['id']) ) {
              ++$updates;
              $this->getFastlane($app,$vercode);
            }
          } else {
            $this->log->info( $app['id']." up-to-date." );
          }
          break;
      case 'github-release-manual':
          if (in_array($run_mode,['manual','all'])) {
            $gh = $this->getGithubLastRel($app);
            if ( !array_key_exists('rel',$gh) ) $gh['rel'] = '{unknown}';
            ( empty($app['manualhint']) ) ? $extra = '' : $extra = "\n- ".$app['manualhint'];
            $this->log->info( $app['id'].": ".$app['ver']." (".$app['filetime'].") / ".$gh['rel'].", manually check ".$app['url'].$extra );
          }
          break;
      case 'direct-link':
          if (!in_array($run_mode,['auto','all'])) break;
          if ( empty($app['apkurl']) ) {
            $this->log->error($app['id'].": direct-link without apkUrl, skipping.");
            break;
          }
          $headers = @get_headers($app['apkurl'], 1);
          if ( $headers === false ) {
            $this->log->error( $app['id'].": file (direct-link) not available, server could not be reached.");
            break;
          }
          if ( substr($headers[0],9,3) > 399 ) {
            $this->log->error( $app['id'].": file (direct-link) not available, server reports '".$headers[0]."'");
            break;
          }
          if ( !array_key_exists('Last-Modified',$headers) ) {
            $this->log->error($app['id'].": Server does not give Last-Modified in header, skipping.");
            break;
          }
          $filedate = strtotime($headers['Last-Modified']);
          if ( $app['filetimestamp'] < $filedate ) {
            $fn = $this->repoDir.'/repo/'.$app['id'].'_'.$filedate.'.apk';
            if ( $vercode = $this->processDownload($fn, $app['apkurl'], $app['id']) ) {
              ++$updates;
              $this->getFastlane($app,$vercode);
            }
          } else {
            $this->log->info( $app['id']." up-to-date." );
          }
          break;
      case 'gone':
          if (in_array($run_mode,['gone','all'])) {
            $this->log->info( $app['id'].": source or APK gone" );
          }
          break;
      case 'none':
          if (in_array($run_mode,['none','all'])) {
            ( empty($app['manualhint']) ) ? $extra = '' : $extra = "\n- ".$app['manualhint'];
            $this->log->info( $app['id'].": auto-update disabled".$extra );
          }
          break;
      default              :
          if (in_array($run_mode,['manual','all'])) {
            if ( empty($app['src']) ) {
              if ( !empty($app['apkurl']) ) $checksrc = $app['apkurl'];
              else $checksrc = $app['url'];
            } else {
              $checksrc = $app['src'];
            }
            ( empty($app['manualhint']) ) ? $extra = '' : $extra = "\n- ".$app['manualhint'];
            if ( !empty($app['apkurl']) ) $extra .= "\n- ".$app['apkurl'];
            $this->log->info( $app['id'].": ".$app['ver']." (".$app['filetime']."), no valid check method; manually check $checksrc".$extra );
          }
          break;
    }

    return $updates;
  }


// ----------------------------------------------------------------------------
  /** Check entire repo for available updates and process them
   * @class ApkUpdater
   * @method checkAll
   * @param optional string run_mode    all (for manual run of a single package),auto (default),build,epo,manual,gone
   * @param optional string redir whether to suppress output of external calls in post-processing ('-q', default) or not ('')
   */
  public function checkAll($run_mode='auto',$redir='-q') {
    $this->log->debug("Full Repo Check started (mode=${run_mode})");
    if ( empty($this->repoData) ) $this->getRepoData();
    $updates = 0;
    foreach ($this->repoData as $app) {
      if ( $this->checkPackage($app,$run_mode) ) ++$updates;
    }
    $this->log->info( "${updates} package(s) updated." );
    if ( $updates > 0 ) $this->postProcess($redir);
  }


// ----------------------------------------------------------------------------
  /** Post-Process: Update repo-index and sync data
   *  Usually only run if we've got some updates.
   * @class ApkUpdater
   * @method postProcess
   * @param optional string redir whether to suppress output of external calls ('-q', default) or not ('')
   */
  public function postProcess($redir='-q') {
    $repoDir = $this->repoDir;
    $fdroidserver = $this->fdroidserver;
    if ( chdir($repoDir) ) {
      $this->log->info( "Updating repository index …" );
      passthru("${fdroidserver} update ${redir}", $rc);
      if ( $rc==0 ) {
        if ($this->conf['general']['purgeArchive']) {
          $this->log->info( "Removing purged APK files from archive …" );
          passthru("rm -f ${repoDir}/archive/*.apk");
          passthru("rm -f ${repoDir}/archive/icons*/*");
        }
        $this->log->info( "Synchronizing remote repository …" );
        passthru("${fdroidserver} server update ${redir}", $rc);
        if ( $rc!=0 ) $this->log->error( "!! Sync to remote repository failed." );
      } else {
        $this->log->error( "!! Something went wrong: 'fdroid update' exit code '${rc}'. Not syncing to remote." );
        return;
      }
      $this->log->info( "Synchronizing APKChecker database to remote location …" );
      passthru("rsync -Paqe ssh ${repoDir}/apkcheck.db ".$this->conf['general']['remoteBase']."/apkcheck.db", $rc);
      if ( $rc!=0 ) $this->log->error( "!! Syncing APKChecker database failed." );
      $this->log->info( "Synchronizing Metadata to remote location …" );
      //passthru("rsync -Paqe ssh ${repoDir}/metadata/ ".$this->conf['general']['remoteBase']."/metadata/", $rc);
      passthru("rsync -qlptgodme ssh --delete ${repoDir}/metadata/*.yml ".$this->conf['general']['remoteBase']."/metadata/", $rc);
      if ( $rc!=0 ) $this->log->error( "!! Syncing Metadata failed." );
    } else {
      $this->log->error( "!! Could not change to repoDir '${repoDir}', index not updated and nothing sync'd." );
    }
  }

}
?>