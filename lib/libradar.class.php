<?php
/** Using Python-based LibRadar https://github.com/pkumza/LibRadar/ to gather
 *  information on libraries used by an APK file
 * @class libradar
 * @author Izzy (izzysoft AT qumran DOT org)
 * @copyright Andreas Itzchak Rehberg (GPLv2)
 * @verbatim
 *  The library definitions shipping with LibRadar are supplemented via 3 other
 *  definition files:
 *  * libradar.txt      : see libradar::libsFile
 *  * libradar_wild.txt : see libradar::libsWildFile
 *  * libsmali.txt      : see libradar::libsSmaliFile
 *  Example use:
 *  $radar = new libradar('/path/to/LibRadar/main',$libsfile,$libsfile_wild,$libsfile_smali);
 *  $radar->debug = 1;
 *  print_r($radar->scan($apk));
 */
class libradar {

  /** directory where LibRadar's main.py can be found
   * @class libradar
   * @attribute protected str radarDir
   */
  protected $radarDir = '';

  /** location of additional library definitions.
   *  by default, we're looking for libradar.txt in the current directory.
   *  This file holds definitions of libraries not (yet) known to LibRadar;
   *  the 'pn' is taken as-is.
   * @class libradar
   * @attribute protected str libsFile
   * @verbatim
   *  Each line in this file is a separate JSON record with the structure
   *  '{"pn":"","lib":"","tp":"","ch":"","dn:,"cpn":""}'
   *  Field names are taken from LibRadar's definitions.
   *  * pn : package name to match the library by, eg. "com/google/protobuf/"
   *  * lib: Name of the library (to present to the user), eg. "Protocol Buffers"
   *  * tp : type; one of "UI Component", "Development Framework", "Utility",
   *         "Development Aid", "Social Network", "Advertisement", "App Market",
   *         "Mobile Analytics", "Payment", "Game Engine", "Map"
   *         (defined by LibRadar)
   *  * ch : Code Homepage (or rather project homepage), an URL
   *  * dn : Repetitions (the number of the library of just the **same version**!)
   *  * cpn: package name of where this was referenced from/found in
   *  For more details, please se the LibRadar documentation.
   */
  protected $libsFile = 'libradar.txt';

  /** location of additional library definitions by "wildcard".
   *  by default, we're looking for libradar_wild.txt in the current directory.
   *  This file holds "wildcarded" definitions of libraries LibRadar does not
   *  (fully) cover; 'pn' is interpreted as "starts-with" (e.g. 'foo/bar/'
   *  would match all libraries starting with 'foo/bar/', like 'foo/bar/baz').
   *  Each line in this file is a separate JSON record with the structure
   *  '{"pn":"","lib":"","tp":"","ch":""}' (see this::libsFile for field names)
   * @class libradar
   * @attribute protected str libsWildFile
   */
  protected $libsWildFile = 'libradar_wild.txt';

  /** location of additional library definitions to compare Smali output on.
   *  by default, we're looking for libsmali.txt in the current directory.
   * @class libradar
   * @attribute protected str libsWildFile
   * @verbatim
   *  Each line in this file is a separate JSON record with the structure
   *  {"id":"","path":"","name":"","type":"","perms":"","url":""}
   *  * id   : package name identifying this library (see 'pn' at this::libsFile)
   *  * path : path to match in the Smali output (like 'pn' in this::libsWildFile)
   *  * name : name of the library (like 'lib' in this::libsFile)
   *  * type : see 'tp' in this::libsFile
   *  * perms: currently unused
   *  * url  : see 'ch' in this::libsFile
   *  'id' and 'path' have a leading slash here. Example record:
   *  {"id":"/adswizz","path":"/adswizz","name":"AdsWizz","type":"Advertisement","perms":"","url":"http://www.adswizz.com/"}
   *  One library (id) can have multiple records differing in the 'path' to match.
   */
  protected $libsSmaliFile = 'libsmali.txt';

  /** Array of additional library definitions loaded from self::libsFile
   * @class libradar
   * @attribute protected array $libs
   */
  protected $libs = [];

  /** Array of additional library definitions loaded from self::libsWildFile
   * @class libradar
   * @attribute protected array $libsWild
   */
  protected $libsWild = [];

  /** Array of additional library definitions loaded from self::libsSmaliFile
   * @class libradar
   * @attribute protected array $libsWild
   */
  protected $libsSmali = [];

  public $debug = 0;

  /** Load library definitions
   * @class libradar
   * @method loadLibs
   * @param opt str type    'static' for static definitions (default), 'wild' for wildcards, 'smali' for our own definitions
   * @param opt str file    name of the file holding the resp. definitions. Defaults to the one matching the type.
   * @return bool success
   */
  public function loadLibs($type='static',$file='') {
    $type = strtolower($type);
    if ( !in_array($type,['static','wild','smali']) ) {
      trigger_error("libradar::loadLibs: unsupported type '$type', definitions not loaded",E_USER_WARNING);
      return FALSE;
    }
    if ( empty($file) ) switch($type) {
      case 'wild'  : $file = $this->libsWildFile; break;
      case 'smali' : $file = $this->libsSmaliFile; break;
      case 'static':
      default      : $file = $this->libsFile; break;
    }
    if ( !file_exists($file) ) {
      trigger_error("libradar::loadLibs: file '$file' does not exist, cannot load libraries.",E_USER_WARNING);
      return FALSE;
    }
    $dict = [];
    foreach( file($file) as $line ) {
      if ( $item = json_decode($line) ) {
        if ( $type=='smali' ) $dict[$item->path] = $item;
        else $dict[$item->pn] = (array) $item;
      }
    }
    switch($type) {
      case 'wild'  : $this->libsWild = $dict; break;
      case 'smali' : $this->libsSmali = $dict; break;
      case 'static':
      default      : $this->libs = $dict; break;
    }
    if ( $this->debug ) echo "libradar::loadLibs: Loaded $type entries from '$file'\n";
    return TRUE;
  }

  /** Setting up the class instance
   * @constructor libradar
   * @param str     radarDir        directory where LibRadar's main.py can be found
   * @param opt str libsFile        location of additional library definitions (default: libradar.txt in current dir)
   * @param opt str libsWildFile    location of additional library definitions by "wildcard" (default: libradar_wild in current dir)
   * @param opt str libsSmaliFile   location of additional library definitions for our own Smali scan
   * @return bool success
   */
  function __construct($radarDir,$libsFile='',$libsWildFile='',$libsSmaliFile=NULL) {
    if ( !is_dir($radarDir) ) {
      trigger_error("libradar: directory '$radarDir' does not exist, cannot run LibRadar!",E_USER_ERROR);
      return FALSE;
    }
    $this->radarDir = $radarDir;
    if ( $this->debug ) echo "libradar: initialized with LibRadar directory '$radarDir'\n";
    if ( !empty($libsFile) && file_exists($libsFile) ) {
      $this->libsFile = $libsFile;
      $this->loadLibs('static',$libsFile);
    }
    if ( !empty($libsWildFile) && file_exists($libsWildFile) ) {
      $this->libsWildFile = $libsWildFile;
      $this->loadLibs('wild',$libsWildFile);
    }
    if ( $libsSmaliFile !== NULL ) $this->libsSmaliFile = $libsSmaliFile;
    if ( !empty($libsSmaliFile) && file_exists($libsSmaliFile) ) {
      $this->loadLibs('smali','');
    }
    return TRUE;
  }

  /** Scan Smali results for additional libraries
   * @class libradar
   * @method scanSmali
   * @param str file    file with directory listing from Smali output (generated by "ls -RD  | egrep '^\.' |grep smali")
   * @return array
   */
  public function scanSmali($file) {
    $res = [];
    if ( empty($this->libsSmali) || !file_exists($file) ) return $res;
    $dirlist = file_get_contents($file);
    foreach($this->libsSmali as $def) {
      if ( preg_match('!^\./smali([^/])*' . $def->path . '([/:])!ims',$dirlist) ) {
        $res[] = ['name'=>$def->name, 'pkgname'=>$def->id, 'type'=>$def->type, 'perms'=>[], 'url'=>$def->url];
      }
    }
    return $res;
  }

  /** Scan an APK for libraries
   * @class libradar
   * @method scan
   * @param str apk file            name of the APK to scan (incl. path if needed)
   * @param str reportUnknownLibs   what to do when encounter an unidentified lib: 'none' (nothing, default), 'echo' (to STDOUT)
   * @return array
   */
  public function scan($apk,$reportUnknownLibs='none') {
    if ( !file_exists($apk) ) {
      trigger_error("libradar:scan: Cannot scan '$apk', file not found.",E_USER_WARNING);
      return [];
    }
    if ( !in_array($reportUnknownLibs,['none','echo']) ) $reportUnknownLibs='none';
    if ( $this->debug ) $reportUnknownLibs='echo';

    // Call LibRadar and capture it's output
    $cmd = 'python '.$this->radarDir.'/main.py';
    if ( $this->debug ) echo "libradar::scan: Calling '$cmd $apk'\n";
    $res = `$cmd $apk 2>/dev/null`;

    // scan the Smali code for our local definitions
    $smali = $this->scanSmali( '/tmp/'.basename($apk).'.dirlist' );
    if ( empty($smali) && $this->debug ) echo "libradar::scan: no items found by SmaliScan\n";

    // parse LibRadar output and isolate the JSON
    preg_match_all('!--Splitter--\s(.+?)\s--Splitter--!ims',$res,$matches);
    $json = $matches[1][0];
    if ( empty($json) ) {
      if ( $this->debug ) echo "libradar::scan: no items found by LibRadar\n";
      return $smali;
    }

    // Still here? OK, let's evaluate what we've got:
    $items = json_decode($json);
    $mc = count($items);
    if ( $this->debug ) echo "libradar::scan: $mc items found\n";
    $libs = [];         // our results go here
    $matchedLibs = [];  // for libsWild matches (we need to merge permissions on those)

    foreach ( $items as $item ) { // walk the result array
      foreach ( ['lib','pn','tp','p','ch'] as $prop )
        ( property_exists($item,$prop) ) ? ${$prop} = $item->$prop : ${$prop} = '';
      if ( empty($lib) ) {      // LibRadar found a library id cannot (yet) identify, so check our additions
        if ( !empty($pn) && isset($this->libs[$pn]) ) { // check our static entries
          $lib = $this->libs[$pn]['lib'];
          if ( empty($tp) ) $tp = $this->libs[$pn]['tp'];
          if ( empty($ch) ) $ch = $this->libs[$pn]['ch'];
        } else {                // we don't have a direct hit either, let's check our wildcards
          $d = dirname($pn);
          while ( !empty($d) && $d != '.' ) {       // walk the package name level by level
            if ( isset($this->libsWild["$d/"]) ) {  // is it in our wildcarded libs?
              if ( isset($matchedLibs["$d/"]) ) {   // if this wildcard already matched before, we need to merge permissions
                $matchedLibs["$d/"] = array_unique(array_merge($matchedLibs["$d/"],$p));
                continue 2;
              }
              // still here? So it didn't match before, let's collect details
              $pn = "$d/"; $lib = $this->libsWild["$d/"]['lib'];
              if ( empty($tp) ) $tp = $this->libsWild["$d/"]['tp'];
              if ( empty($ch) ) $ch = $this->libsWild["$d/"]['ch'];
              $matchedLibs["$d/"] = $p;
              break;            // we've got all we need for this one
            }
            $d = dirname($d);   // eliminate one level from the end of the package name to continue
          }
          // We didn't find anything either, so skip to the next entry
          if ( empty($lib) ) {
            if ( $reportUnknownLibs=='echo' ) echo "UNKNOWN: ".str_replace('\/','/',json_encode($item))."\n";
            continue;
          }
        }
      } // end of check against our own definitions

      // now let's add our findings to the return list
      $pn = preg_replace('!/$!','',$pn);
      if ( isset($libs[$pn]) ) $libs[$pn]['perms'] = array_unique(array_merge($libs[$pn]['perms'],$p));
      else $libs[$pn] = [
        'name' => $lib,     /* Name of the library */
        'pkgname' => $pn,   /* package name of the library */
        'type' => $tp,      /* Type, e.g. Utility, Development Aid, Social Network, Advertisement, App Market, Mobile Analytics, Payment, UI Component, Game Engine, Map
                             * we added: Cloud Storage (e.g. Dropbox) */
        'perms' => $p,      /* array[0..n] of permissions (tech_name) */
        'url' => $ch        /* URL for detailed info */
      ];
      // other props (skipped): dn (repetitions), bh (B_Hash), btc (B_Total_Call), btn (B_Total_Number), cpn (canonical package name???), sp (Simplified Path), csp (Current S_path)
    }

    // update permissions for merged wildcard libs
    if ( !empty($matchedLibs) ) foreach ($libs as $num=>$lib) if ( isset($matchedLibs[$lib['pkgname']]) ) $libs[$num]['perms'] = $matchedLibs[$lib['pkgname']];

    // merge with our Smali results (if any)
    foreach ( $smali as $sitem ) {
      $sitem['pkgname'] = substr($sitem['pkgname'],1); // trim leading slash
      if ( !array_key_exists($sitem['pkgname'], $libs) ) $libs[$sitem['pkgname']] = $sitem;
    }

    // that's it – return our results!
    return $libs;
  }
}
?>