#!/usr/bin/php
<?php
// ============================================================================
// ApkReScanner CLI
// © Andreas Itzchak Rehberg
// ----------------------------------------------------------------------------
// Purpose of this script is to manually rescan a given APK file after
// library details have been updated in lib/libsmali.txt, or to trigger a
// rescan at VirusTotal. This does not sync the DB, use apkScan.php for that.
// ----------------------------------------------------------------------------
// This program is free software; you can redistribute and/or modify it under
// the terms of the GNU General Public License (see ../LICENSE)
// ============================================================================

// ==========================================================[ CLI options ]===
$opts = getopt('qvarf:h');
$scriptargs = $argv;

// -----------------------------------------------------------=[ Show Help }=--
function syntax() {
  GLOBAL $scriptargs;
  echo "\n";
  echo "Syntax:\n";
  echo " $scriptargs[0] [-q|-v] [-a|-r] [-t <days>] -f <apk_file_name>\n";
  echo " $scriptargs[0] -h\n";
  echo "   a: run all scans (radar & VT)\n";
  echo "   f <file>: specify the APK file to rescan\n";
  echo "   h: show this help\n";
  echo "   r: run radar scan (local library scan) only\n";
  echo "   q: be quiet (only log warnings or higher to screen)\n";
  echo "   t: timeframe (in days) for the age of a VT report before forcing a scan (default: 0)\n";
  echo "   v: be verbose (debug log to screen)\n";
  echo "\n";
}
if ( isset($opts['h']) ) {
  syntax();
  exit;
}

// ========================================================[ initial setup ]===
require_once('../lib/apkcheck.class.php');
require_once('../lib/class.logging.php'); // also defines the constants, so we need this first
$conf = parse_ini_file('../lib/apkscan.ini', TRUE); // parse with sections
$apkcheck = new apkcheck($conf['general']['apkdir'],$conf['general']['database']);
$apkcheck->initRadar($conf['libradar']['radarDir'],$conf['libradar']['libsFile'],$conf['libradar']['libsWildFile'],$conf['libradar']['libsSmaliFile'],'echo');
$apkcheck->initVT($conf['virustotal']['apikey']);

// -------------------------------------------------------------=[ Logging }=--
if ( isset($opts['q']) ) $screenlog = WARNING;
elseif ( isset($opts['v']) ) $screenlog = DEBUG;
else $screenlog = INFO;
$filelog   = NOLOG;
$logfile   = '';
// log params: filename (''), fileloglevel(INFO), screenloglevel(INFO) / EMERGENCY, CRITICAL, ALERT, ERROR, WARNING, NOTICE, INFO, DEBUG, NOLOG
$log = new logging($logfile,$filelog,$screenlog);

// ---------------------------------------------=[ Check parameters passed ]=--
if ( !isset($opts['f']) ) { $log->error("No file specified. Pass '-f <filename>' to the script."); syntax(); exit; }
elseif ( empty($opts['f']) ) { $log->error("'-f' expects a file name, but we got nothing."); syntax(); exit; }
elseif ( !file_exists($conf['general']['apkdir'].'/'.$opts['f']) ) { $log->error("Specified file '".$opts['f']."' not found in '".$conf['general']['apkdir']."'"); exit; }
// what scanners to run
if ( isset($opts['a']) ) { $scan['radar'] = true; $scan['vt'] = true; }
elseif ( isset($opts['r']) ) { $scan['radar'] = true; $scan['vt'] = false; }
else { $scan['radar'] = false; $scan['vt'] = true; }

// ===========================================[ Do it: ReSchedule the file ]===
$file = $conf['general']['apkdir'].'/'.$opts['f'];
$pkgname = trim(preg_replace("!package: name='(.+?)'.*!",'\1',`aapt d badging $file | head -n 1`));
$log->info("Processing '$file' ($pkgname)");
if ($scan['radar']) $apkcheck->radarFile($opts['f']);
if ($scan['vt']) {
  $apkcheck->newFile($pkgname,$opts['f']);
  if ( isset($opts['t']) ) $days = $opts['t']; else $days = 0;
  $rc = $apkcheck->vtRescan($opts['f'],'',$days); // force rescan
  switch($rc) {
    case 0: $log->info("- VirusTotal rescan scheduled"); break;
    case 1: $log->info("- VirusTotal rescan scheduled, older results available"); break;
    default: $log->warn("! failed to initialize VirusTotal rescan (response code: $rc)"); break;
  }
}
?>