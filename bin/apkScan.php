#!/usr/bin/php
<?php
// ============================================================================
// ApkScanner CLI
// © Andreas Itzchak Rehberg
// ----------------------------------------------------------------------------
// This program is free software; you can redistribute and/or modify it under
// the terms of the GNU General Public License (see ../LICENSE)
// ============================================================================

// ==========================================================[ CLI options ]===
$opts = getopt('qv');

// ========================================================[ initial setup ]===
require_once('../lib/apkcheck.class.php');
require_once('../lib/class.logging.php'); // also defines the constants, so we need this first
$conf = parse_ini_file('../lib/apkscan.ini', TRUE); // parse with sections
$apkcheck = new apkcheck($conf['general']['apkdir'],$conf['general']['database']);
$apkcheck->initRadar($conf['libradar']['radarDir'],$conf['libradar']['libsFile'],$conf['libradar']['libsWildFile'],$conf['libradar']['libsSmaliFile'],'echo');
$apkcheck->initVT($conf['virustotal']['apikey']);

// -------------------------------------------------------------=[ Logging }=--
if ( isset($opts['q']) ) $screenlog = WARNING;
elseif ( isset($opts['v']) ) $screenlog = DEBUG;
else $screenlog = INFO;
$filelog   = NOLOG;
$logfile   = '';
// log params: filename (''), fileloglevel(INFO), screenloglevel(INFO) / EMERGENCY, CRITICAL, ALERT, ERROR, WARNING, NOTICE, INFO, DEBUG, NOLOG
$log = new logging($logfile,$filelog,$screenlog);

// =====================================================[ helper functions ]===
/** Check whether there are APK files we don't have in our database yet
 * @function scan4new
 * @param ref array files   List of files from APK location (globbed, w/ path)
 * @param ref array conf    Our .ini file as array by parse_ini_file
 */
function scan4new(&$files,&$conf) {
  ( empty($conf['general']['maxscan']) ) ? $max = 25 : $max = $conf['general']['maxscan'];
  $i = 0;
  foreach ( $files as $file ) {
    if ( $i == $max ) break;
    $filename = basename($file);
    $info = $GLOBALS['apkcheck']->getFile( $filename );
    if ( empty($info) || empty($info['apk_libraries']) ) {
      ++$i;
      $pkgname = trim(preg_replace("!package: name='(.+?)'.*!",'\1',`aapt d badging $file | head -n 1`));
      $GLOBALS['log']->info("Processing '$file' ($pkgname) as #$i");
      $GLOBALS['apkcheck']->newFile($pkgname,$filename);
    }
  }
}

/** Scan queued files with VirusTotal
 * @function scanQ
 */
function scanQ() {
  $q = $GLOBALS['apkcheck']->getQ('IDLE');
  $qc = count($q);
  $GLOBALS['log']->info("Files in queue for VirusTotal: $qc");
  $db = new apkcheck_db('../apkcheck.db'); // for library check
  for ( $i=0; $i < $GLOBALS['conf']['general']['maxscan'], $i < $qc; ++$i ) {
    $file = $q[$i]['local_file_name'];
    $entry = $q[$i]['entry_time'];
    ( empty($q[$i]['vt_json']) ) ? $hash = '' : $hash = json_decode($q[$i]['vt_json'])->scan_id;
    $start = time();
    $rc = $GLOBALS['apkcheck']->vtFile($file,$hash);
    $ela = time() - $start;
    $res = ['1'=>'results obtained.','0'=>'file enqueued.','-1'=>'VirusTotal error!','-91'=>'error obtaining results!','-90'=>'error enqueueing file!','-99'=>'got empty response from VT'];
    ( isset($res[$rc]) ) ? $note = $res[$rc] : $note = "unknown error: $rc!";

    // check for libraries
    $db->connect();           // must initialize to set timeout
    $db->busyTimeout('5000'); // wait 5s when DB is locked
    $libs = $db->getFile($file);
    $ads = json_decode($libs['apk_admodules']);
    $anal= json_decode($libs['apk_analyticsmodules']);
    $note .= '; AdMods: '.count($ads).', AnalyticMods: '.count($anal);
    $db->disconnect();

    $GLOBALS['log']->info("- processed '$file' (enqueued at ".date('Y-m-d H:i:s',$entry).") in $ela seconds: $note");
    ( $rc===0 ) ? $multi = 30 : $multi = 16; // enqueuing files takes 2 requests, so double it
    $gap = ($multi - $ela)*1000000; // We have a public key, so Request rate: max 4 requests/minute (1/4 min rounded up to 16s here as $ela might be rounded down)
    if ( $i+1 < $GLOBALS['conf']['general']['maxscan'] && $i+1 < $qc && $gap > 0 ) usleep( $gap );
  }
  $q = $GLOBALS['apkcheck']->getQ('IDLE');
  $GLOBALS['log']->info('Files remaining in queue for VirusTotal: '.count($q));
}

/** Remove DB entries which point to files that no longer exist
 * @function removeDeleted
 * @param ref array files   List of files from APK location (globbed, w/ path)
 */
function removeDeleted(&$files) {
  $filenames = [];
  foreach ($files as $file) $filenames[] = basename($file);
  $GLOBALS['log']->info("Files: ".count($filenames) . ", DB-Entries: ".count($GLOBALS['apkcheck']->listFileNames()));
  foreach ($GLOBALS['apkcheck']->listFileNames() as $file) {
    if ( in_array($file,$filenames) ) continue;
    $GLOBALS['log']->notice("File '$file' no longer exists, purging it from DB");
    if ( !$GLOBALS['apkcheck']->deleteFile($file) ) $GLOBALS['log']->warn("Could not delete '$file' (DB error)");
  }
  $GLOBALS['apkcheck']->compressDB();
}


// =================================================================[ MAIN ]===
$files = glob($conf['general']['apkdir']."/*.apk");
scan4new($files,$conf);
scanQ();
removeDeleted($files);
$log->info( "Synchronizing APKChecker database to remote location …" );
passthru("rsync -Paqe ssh ../apkcheck.db ".$conf['general']['remoteBase']."/apkcheck.db", $rc);

$infectionReport = "sqlite3 ".$conf['general']['database']." < ".__DIR__."/getInfected.sql";
passthru($infectionReport);

?>