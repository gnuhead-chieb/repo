-- report from up to 2 (from 60+) engines are considered "false positives"
-- and not reported to us (though marked for the users)
-- also exclude files reported by more engines which we have verified directly
SELECT local_file_name,vt_detected||' malware alerts',libcount||' libs'
  FROM files
 WHERE vt_detected > 2
   AND local_file_name NOT IN (
         'foo' -- dummy
        ,'com.nowsecure.android.vts_13.apk' -- com.nowsecure.android.vts  False positive? https://github.com/AndroidVTS/android-vts/issues/135 & https://www.virustotal.com/de/file/cdbcdc0ad48ff2dbb0df5440fd62707d5a929d949f42fbbc694e7f7a022b57f8/analysis/1470893391/
        ,'com.piapps.flashcardpro_10.apk' -- PUA, PUA…
       );
