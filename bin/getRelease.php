#!/usr/bin/php
<?php
// ============================================================================
// Update Checker and Retriever
// © Andreas Itzchak Rehberg
// ----------------------------------------------------------------------------
// This is the CLI front-end to the ApkUpdater class. It can be used to check
// for updates for a single app or a group of apps already listed in the repo
// index, depending on the parameters passed (or not passed).
// ----------------------------------------------------------------------------
// This program is free software; you can redistribute and/or modify it under
// the terms of the GNU General Public License (see ../LICENSE)
// ============================================================================

# ===============================================[ required libraries etc. ]===
require_once('../lib/class.logging.php'); // also defines the constants, so we need this first
require_once('../lib/ApkUpdater.class.inc');

# ==================================================[ configuration values ]===
libxml_use_internal_errors(true);
$screenlog = INFO;
//$filelog   = NOLOG;
//$logfile   = '';
$filelog   = WARNING;
$logfile   = '/web/ftp/repo/fdroid/bin/getRelease.log';
$valid_modes = ['auto','build','manual','repo','gone','none','static','all','post'];

# =====================================================[ command line args ]===
$help = <<<EOH
Options :
         -h          = show this help and exit
         -p <pkg>    = only check for this package
         -m <mode>   = modus (auto, build, manual, gone, none, static, all, post)
         -q          = quiet (cron mode: decrease log level to error; has precendence over '-v')
         -v          = verbose (increase log level to debug)
EOH;

$opts = getopt('m:p:qvh');
if ( isset($opts['h']) ) { echo "$help\n"; exit; }
if ( isset($opts['m']) && in_array($opts['m'],$valid_modes) ) $run_mode = $opts['m'];
else $run_mode = 'auto';
if ( isset($opts['q']) ) $screenlog = ERROR;
elseif ( isset($opts['v']) ) $screenlog = DEBUG;
if ( isset($opts['p']) && !empty($opts['p']) ) $pkgname = $opts['p'];
else $pkgname = '';

# =========================================================[ initial setup ]===
// log params: filename (''), fileloglevel(INFO), screenloglevel(INFO) / EMERGENCY, CRITICAL, ALERT, ERROR, WARNING, NOTICE, INFO, DEBUG, NOLOG
$log = new logging($logfile,$filelog,$screenlog);
$updater = new ApkUpdater('../lib/apkscan.ini',$log);


# ==================================================================[ MAIN ]===
# ----------------------------------------------------=[ Check for updates ]=--
($screenlog < INFO) ? $redir = "-q" : $redir = '';
if ( !empty($pkgname) ) {
  $updates = $updater->checkPackage($pkgname,'all');
  if ( $run_mode=='post' && $updates > 0 ) $updater->postProcess($redir);
} elseif ($run_mode == 'post') { // run post-processing only
  $updater->postProcess($redir);
} else {
  $updater->checkAll($run_mode,$redir);
}
?>